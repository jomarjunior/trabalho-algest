# Generated by Django 3.1 on 2020-10-23 00:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Questao',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('_dificuldade', models.PositiveIntegerField(default=0)),
                ('_tipo', models.BooleanField(verbose_name='Marcar como resposta correta')),
                ('_tempo', models.DateTimeField(auto_now_add=True, verbose_name='tempo decorrido')),
                ('_tempoEntrada', models.DateTimeField(auto_now_add=True, verbose_name='tempo de entrada')),
            ],
        ),
        migrations.CreateModel(
            name='Resposta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('_texto', models.CharField(max_length=200)),
                ('_gabarito', models.BooleanField()),
                ('_escolhaAluno', models.BooleanField()),
                ('questao', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='home.questao')),
            ],
        ),
        migrations.CreateModel(
            name='Pergunta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('_texto', models.TextField(max_length=1024)),
                ('questao', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='home.questao')),
            ],
        ),
    ]
