from django.contrib import admin

# Register your models here.

from .models import Questao
from .models import Pergunta
from .models import Resposta

class PerguntaInline ( admin.TabularInline ):
    model = Pergunta
    max_num = 1
    min_num = 1
    can_delete = False

class RespostaInline ( admin.TabularInline ):
    model = Resposta
    max_num = 4
    min_num = 4
    can_delete = False
    fieldsets = [
            (None, {'fields': ['texto', 'gabarito']}),
            ]

class QuestaoAdmin ( admin.ModelAdmin ):
    fieldsets = [
            ('Dados da Questao', {'fields': ['dificuldade', 'tipo']}),
            ]
    inlines = [PerguntaInline, RespostaInline]

admin.site.register(Questao, QuestaoAdmin)
