from django.db import models

# Create your models here.

class Questao ( models.Model ):
    dificuldade = models.PositiveIntegerField ( default = 0 )
    tipo = models.BooleanField ( 'Marcar como resposta correta' )
    tempo = models.DateTimeField ('tempo decorrido', auto_now_add = True)
    tempoEntrada = models.DateTimeField ('tempo de entrada', auto_now_add = True)

class Pergunta ( models.Model ):
    texto = models.TextField (max_length = 1024)
    questao = models.ForeignKey ( Questao, on_delete = models.CASCADE )

    def __str__ ( self ):
        return self.texto;

    def AlterarTexto ( self, novoValor ):
        self.texto = novoValor

class Resposta ( models.Model ):
    texto = models.CharField (max_length = 200)
    gabarito = models.BooleanField ( )
    escolhaAluno = models.BooleanField ( default = False )
    questao = models.ForeignKey ( Questao, on_delete = models.CASCADE )

    def __str__ ( self ):
        return self.texto

    def AlterarTexto ( self, novoTexto ):
        self.texto = novoTexto

    def DefinirComoCorreta ( self ):
        self.gabarito = True

    def DefinirComoIncorreta ( self ):
        self.gabarito = False
